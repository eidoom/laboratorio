data:extend (
	{
		-- https://en.wikipedia.org/wiki/Torrefaction
		{
			type = "recipe",
			name = "laboratorio-torrefaction",
			category = "smelting",
			energy_required = 3.2,
			ingredients =
			{
				{"wood", 1},
			},
			results =
			{
				{"coal", 1},
			},
		},
	}
	)
